var stateGrid = {
    "ME": { "state": "ME", "row": 0, "col": 10 },
    "WI": { "state": "WI", "row": 1, "col": 5 },
    "VT": { "state": "VT", "row": 1, "col": 9 },
    "NH": { "state": "NH", "row": 1, "col": 10 },
    "WA": { "state": "WA", "row": 2, "col": 0 },
    "ID": { "state": "ID", "row": 2, "col": 1 },
    "MT": { "state": "MT", "row": 2, "col": 2 },
    "ND": { "state": "ND", "row": 2, "col": 3 },
    "MN": { "state": "MN", "row": 2, "col": 4 },
    "IL": { "state": "IL", "row": 2, "col": 5 },
    "MI": { "state": "MI", "row": 2, "col": 6 },
    "NY": { "state": "NY", "row": 2, "col": 8 },
    "MA": { "state": "MA", "row": 2, "col": 9 },
    "OR": { "state": "OR", "row": 3, "col": 0 },
    "NV": { "state": "NV", "row": 3, "col": 1 },
    "WY": { "state": "WY", "row": 3, "col": 2 },
    "SD": { "state": "SD", "row": 3, "col": 3 },
    "IA": { "state": "IA", "row": 3, "col": 4 },
    "IN": { "state": "IN", "row": 3, "col": 5 },
    "OH": { "state": "OH", "row": 3, "col": 6 },
    "PA": { "state": "PA", "row": 3, "col": 7 },
    "NJ": { "state": "NJ", "row": 3, "col": 8 },
    "CT": { "state": "CT", "row": 3, "col": 9 },
    "RI": { "state": "RI", "row": 3, "col": 10 },
    "CA": { "state": "CA", "row": 4, "col": 0 },
    "UT": { "state": "UT", "row": 4, "col": 1 },
    "CO": { "state": "CO", "row": 4, "col": 2 },
    "NE": { "state": "NE", "row": 4, "col": 3 },
    "MO": { "state": "MO", "row": 4, "col": 4 },
    "KY": { "state": "KY", "row": 4, "col": 5 },
    "WV": { "state": "WV", "row": 4, "col": 6 },
    "VA": { "state": "VA", "row": 4, "col": 7 },
    "MD": { "state": "MD", "row": 4, "col": 8 },
    "DE": { "state": "DE", "row": 4, "col": 9 },
    "AZ": { "state": "AZ", "row": 5, "col": 1 },
    "NM": { "state": "NM", "row": 5, "col": 2 },
    "KS": { "state": "KS", "row": 5, "col": 3 },
    "AR": { "state": "AR", "row": 5, "col": 4 },
    "TN": { "state": "TN", "row": 5, "col": 5 },
    "NC": { "state": "NC", "row": 5, "col": 6 },
    "SC": { "state": "SC", "row": 5, "col": 7 },
    "OK": { "state": "OK", "row": 6, "col": 3 },
    "LA": { "state": "LA", "row": 6, "col": 4 },
    "MS": { "state": "MS", "row": 6, "col": 5 },
    "AL": { "state": "AL", "row": 6, "col": 6 },
    "GA": { "state": "GA", "row": 6, "col": 7 },
    "HI": { "state": "HI", "row": 7, "col": 0 },
    "AK": { "state": "AK", "row": 7, "col": 1 },
    "TX": { "state": "TX", "row": 7, "col": 3 },
    "FL": { "state": "FL", "row": 7, "col": 8 }
};

var selectedStates = {};
var shade1 = { "ME": true, "VT": true, "FL": true, "HI": true, "AK": true, "MS": true };
var shade2 = { "AL": true, "NC": true };
var shade3 = { "KY": true, "OH": true };
var shade4 = { "IA": true, "GA": true };
var shade5 = { "PA": true, "IL": true };

var stateIDs = Object.getOwnPropertyNames(stateGrid);

var height = 500;
var width = 800;

var svg = d3.select("#plot").append("svg")
    .attr("height", height)
    .attr("width", width);

var stateWidth = 53;
var gap = 2;

var stateGroup = svg.append("g").attr("transform", "translate(50, 50)");
var stateXScale = d3.scaleLinear().domain([0, 11]).range([0, 11 * (stateWidth + gap)]);
var stateYScale = d3.scaleLinear().domain([0, 7]).range([0, 7 * (stateWidth + gap)]);

var stateGroups = stateGroup.selectAll("g").data(stateIDs)
    .enter().append("g")
    .attr("transform", function (stateID) { return "translate(" + stateXScale(stateGrid[stateID].col) + "," + stateYScale(stateGrid[stateID].row) + ")"; });

var stateRects = stateGroups.append("rect")
    .attr("width", stateWidth).attr("height", stateWidth)

    // .style("fill", function (stateID) { if (selectedStates[stateID]) { return "#99f"; } else { return "#f4f5fa"; } })
    // .style("stroke", function (stateID) { if (selectedStates[stateID]) { return "#99f"; } else { return "#697dc1"; } })
    .style("fill", function (stateID) {
        let opacity = getOpacity(stateID) + 0.5;
        if(opacity) {
            return "rgba(105,179,162," + opacity + ")";
        }
        return "#f4f5fa";
    });


stateRects
    .on("click", (stateID) => {


        console.log(stateID);
        drawHexbins(stateID);
        drawScatters(stateID);
        // select (or unselect) the state
        selectedStates[stateID] = !selectedStates[stateID];

        let el = document.getElementById("step_2");
        el.scrollIntoView({ behavior: "smooth" });
        el.style = "visibility: block";

        // Update the color of the state box
        // stateRects.style("fill", function (stateID) { if (selectedStates[stateID]) { return "#99f"; } else { return "#ccc"; } });
        // stateRects.style("stroke", function (stateID) { if (selectedStates[stateID]) { return "black"; } });
        // this.selectState.emit(selectedStates[stateID]);
    });

// What do we have to do to make this not clickable?
stateGroups.append("text")
    .attr("class", "stateID")
    .style("font-size", "x-small")
    .style("pointer-events", "none")
    .attr("x", stateWidth / 2)
    .attr("y", stateWidth / 2)
    .text(function (d) { return d; });


function getOpacity(stateId){
    let opacity = stateMap[stateId];
    console.log(opacity);
    return opacity;
}
