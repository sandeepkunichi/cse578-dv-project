function drawRatingsView(offering_id){
    let colors = Highcharts.getOptions().colors;
    let dataToLoad = ratingsData[offering_id];
    Highcharts.chart('ratings_dist', {

        chart: {
            type: 'streamgraph',
            marginBottom: 30,
            zoomType: 'x'
        },

        credits: {
            enabled: false
        },

        // Make sure connected countries have similar colors
        colors: [
            "#008b00",
            "#32a232",
            "#66b966",
            "#99d099",
            "#cce7cc"
        ],

        title: {
            floating: true,
            align: 'left',
            text: 'Ratings Distribution'
        },

        xAxis: {
            maxPadding: 0,
            type: 'category',
            crosshair: true,
            labels: {
                align: 'left',
                reserveSpace: false,
                rotation: 270
            },
            lineWidth: 0,
            margin: 20,
            tickWidth: 0
        },

        yAxis: {
            visible: false,
            startOnTick: false,
            endOnTick: false
        },

        legend: {
            enabled: false
        },

        plotOptions: {
            series: {
                label: {
                    minFontSize: 5,
                    maxFontSize: 15,
                    style: {
                        color: 'rgba(255,255,255,0.75)'
                    }
                }
            }
        },

        // Data parsed with olympic-medals.node.js
        series: dataToLoad,

        exporting: {
            sourceWidth: 800,
            sourceHeight: 600
        }

    });
}
