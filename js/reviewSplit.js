function drawReviews(offering_id) {

    let old_vis = d3.select('#review_division');
    old_vis.selectAll('svg').remove();


    console.log(offering_id);
    const width = 300;
    const height = 210;

    var svg = d3.select("#review_division").append("svg")
        .attr("width", width)
        .attr("height", height);

    /* set up the parliament */
    var parliament = d3.parliament();
    parliament.width(width).height(height).innerRadiusCoef(0.5);
    parliament.enter.fromCenter(true).smallToBig(true);
    parliament.exit.toCenter(false).bigToSmall(true);

    /* register event listeners */
    parliament.on("mouseover", function(d) {
        //d3.selectAll('circle').attr('opacity', 0.2);
        d3.select(this).attr('stroke', d => cor(d.party.legend)).attr('stroke-width',4);
    });
    parliament.on("mouseout", function(d) {
        d3.select(this).attr('stroke', d => cor(d.party.legend)).attr('stroke-width',0);
    });


    const cor = d3.scaleOrdinal();

    /* add the parliament to the page */
    d3.json("data/review_division.json", function(d) {
        d = d[offering_id];
        const partidos = d3.map(d, d => d.legend).keys();
        const cores = ['green', 'red'];
        cor.domain(partidos).range(cores);

        svg.datum(d).call(parliament);
        const bolinhas = d3.select("#review_division").selectAll('svg circle').attr('fill', d => cor(d.party.legend))
    });
}
