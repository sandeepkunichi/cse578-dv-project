function drawForOffering(offeringId, name){
    let textForOffering = reviewData[offeringId]['word_data'];
    Highcharts.chart('offering_analysis', {
        chart: {
            type: 'packedbubble',
            height: '100%'
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Text Analysis for ' + name
        },
        tooltip: {
            useHTML: true,
            pointFormat: '<b>{point.name}</b> occurs {point.y} times'
        },
        plotOptions: {
            packedbubble: {
                minSize: '60%',
                maxSize: '120%',
                zMin: 0,
                zMax: 500,
                layoutAlgorithm: {
                    splitSeries: false,
                    gravitationalConstant: 0.02
                },
                dataLabels: {
                    enabled: true,
                    format: '{point.name}',
                    filter: {
                        property: 'y',
                        operator: '>',
                        value: 0
                    },
                    style: {
                        color: 'black',
                        textOutline: 'none',
                        fontWeight: 'normal'
                    }
                }
            }
        },
        series: textForOffering
    });

}
