Highcharts.chart('positive_map', {
    chart: {
        type: 'tilemap',
        inverted: true,
        height: '80%'
    },

    credits: {
        enabled: false
    },

    title: {
        text: 'Positive Hotel Reviews by State'
    },

    xAxis: {
        visible: false
    },

    yAxis: {
        visible: false
    },

    colorAxis: {
        /*
        dataClasses: [{
            from: 0,
            to: 1000000,
            color: '#F9EDB3',
            name: '< 1M'
        }, {
            from: 1000000,
            to: 5000000,
            color: '#FFC428',
            name: '1M - 5M'
        }, {
            from: 5000000,
            to: 20000000,
            color: '#FF7987',
            name: '5M - 20M'
        }, {
            from: 20000000,
            color: '#FF2371',
            name: '> 20M'
        }]
        */
        maxColor: '#42f46e',
        min: 2,
        max: 181830
    },

    tooltip: {
        headerFormat: '',
        pointFormat: 'The number of positive reviews for <b> {point.name}</b> is <b>{point.value}</b>'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.hc-a2}',
                color: '#000000',
                style: {
                    textOutline: false
                }
            },
            point: {
                events: {
                    click: function () {
                        drawHexbins(this.name);
                        drawScatters(this.name);
                    }
                }
            }
        }
    },

    series: positiveStateData
});


Highcharts.chart('negative_map', {
    chart: {
        type: 'tilemap',
        inverted: true,
        height: '80%'
    },

    title: {
        text: 'Negative Hotel Reviews by State'
    },

    credits: {
        enabled: false
    },

    xAxis: {
        visible: false
    },

    yAxis: {
        visible: false
    },

    colorAxis: {
        /*
        dataClasses: [{
            from: 0,
            to: 1000000,
            color: '#F9EDB3',
            name: '< 1M'
        }, {
            from: 1000000,
            to: 5000000,
            color: '#FFC428',
            name: '1M - 5M'
        }, {
            from: 5000000,
            to: 20000000,
            color: '#FF7987',
            name: '5M - 20M'
        }, {
            from: 20000000,
            color: '#FF2371',
            name: '> 20M'
        }]
        */
        maxColor: '#aa1b38',
        min: 2,
        max: 85227
    },

    tooltip: {
        headerFormat: '',
        pointFormat: 'The number of negative reviews for <b> {point.name}</b> is <b>{point.value}</b>'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.hc-a2}',
                color: '#000000',
                style: {
                    textOutline: false
                }
            }
        }
    },

    series: negativeStateData
});
