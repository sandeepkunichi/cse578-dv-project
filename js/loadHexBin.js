function drawHexbin(regionId){

    // set the dimensions and margins of the graph
    var margin = {top: 10, right: 30, bottom: 40, left: 40},
        width = 300 - margin.left - margin.right,
        height = 250 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    var svg = d3.select("#hex_bin")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    // read data
    d3.csv("data/regionwise/offering_with_region_id_" + regionId + ".csv", function(data) {

        let maxX = parseInt(getMax(data, 'x')['x']);
        let maxY = parseInt(getMax(data, 'y')['y']);

        // Add X axis
        var x = d3.scaleLinear()
            .domain([1, maxX + 100])
            .range([ 0, width ]);
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        // Add Y axis
        var y = d3.scaleLinear()
            .domain([1, maxY + 100])
            .range([ height, 0 ]);
        svg.append("g")
            .call(d3.axisLeft(y));

        // Reformat the data: d3.hexbin() needs a specific format
        var inputForHexbinFun = [];
        data.forEach(function(d) {
            inputForHexbinFun.push( [x(d.x), y(d.y)] )  // Note that we had the transform value of X and Y !
        });

        // Prepare a color palette
        var color = d3.scaleLinear()
            .domain([0, 7]) // Number of points in the bin?
            .range(["transparent",  "#69b3a2"]);

        // Compute the hexbin data
        var hexbin = d3.hexbin()
            .radius(10) // size of the bin in px
            .extent([ [0, 0], [width, height] ]);

        // Plot the hexbins
        svg.append("clipPath")
            .attr("id", "clip")
            .append("rect")
            .attr("width", width)
            .attr("height", height);

        svg.append("g")
            .attr("clip-path", "url(#clip)")
            .selectAll("path")
            .data( hexbin(inputForHexbinFun) )
            .enter().append("path")
            .attr("d", hexbin.hexagon())
            .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
            .attr("fill", function(d) { return color(d.length); })
            .attr("stroke", "black")
            .attr("stroke-width", "0.1")

        // text label for the x axis
        svg.append("text")
            .attr("transform",
                "translate(" + (width / 2) + " ," +
                (height + margin.top + 20) + ")")
            .style("text-anchor", "middle")
            .text("Number of positive reviews");

        // text label for the y axis
        svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 0 - margin.left - 5)
            .attr("x",0 - (height / 2))
            .attr("dy", "1em")
            .style("text-anchor", "middle")
            .text("Number of negative reviews");

    });
}


function drawHexbins(state){
    let old_vis = d3.select('#hex_bin');
    old_vis.selectAll('svg').remove();
    old_vis.selectAll('span').remove();
    let regionIds = getRegionsForState(state);
    regionIds.forEach(value => {
        drawHexbin(value)
    });
}


function getMax(arr, prop) {
    var max;
    for (var i=0 ; i<arr.length ; i++) {
        if (!max || parseInt(arr[i][prop]) > parseInt(max[prop]))
            max = arr[i];
    }
    return max;
}
