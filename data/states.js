function abbrState(input, to){

    var states = [
        ['Arizona', 'AZ'],
        ['Alabama', 'AL'],
        ['Alaska', 'AK'],
        ['Arizona', 'AZ'],
        ['Arkansas', 'AR'],
        ['California', 'CA'],
        ['Colorado', 'CO'],
        ['Connecticut', 'CT'],
        ['Delaware', 'DE'],
        ['Florida', 'FL'],
        ['Georgia', 'GA'],
        ['Hawaii', 'HI'],
        ['Idaho', 'ID'],
        ['Illinois', 'IL'],
        ['Indiana', 'IN'],
        ['Iowa', 'IA'],
        ['Kansas', 'KS'],
        ['Kentucky', 'KY'],
        ['Kentucky', 'KY'],
        ['Louisiana', 'LA'],
        ['Maine', 'ME'],
        ['Maryland', 'MD'],
        ['Massachusetts', 'MA'],
        ['Michigan', 'MI'],
        ['Minnesota', 'MN'],
        ['Mississippi', 'MS'],
        ['Missouri', 'MO'],
        ['Montana', 'MT'],
        ['Nebraska', 'NE'],
        ['Nevada', 'NV'],
        ['New Hampshire', 'NH'],
        ['New Jersey', 'NJ'],
        ['New Mexico', 'NM'],
        ['New York', 'NY'],
        ['North Carolina', 'NC'],
        ['North Dakota', 'ND'],
        ['Ohio', 'OH'],
        ['Oklahoma', 'OK'],
        ['Oregon', 'OR'],
        ['Pennsylvania', 'PA'],
        ['Rhode Island', 'RI'],
        ['South Carolina', 'SC'],
        ['South Dakota', 'SD'],
        ['Tennessee', 'TN'],
        ['Texas', 'TX'],
        ['Utah', 'UT'],
        ['Vermont', 'VT'],
        ['Virginia', 'VA'],
        ['Washington', 'WA'],
        ['West Virginia', 'WV'],
        ['Wisconsin', 'WI'],
        ['Wyoming', 'WY'],
        ['District of Columbia', 'DC']
    ];

    if (to == 'abbr'){
        input = input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        for(i = 0; i < states.length; i++){
            if(states[i][0] == input){
                return(states[i][1]);
            }
        }
    } else if (to == 'name'){
        input = input.toUpperCase();
        for(i = 0; i < states.length; i++){
            if(states[i][1] == input){
                return(states[i][0]);
            }
        }
    }
}

let positiveStateData = [{
    name: '',
    data: [{'y': 9, 'name': 'New York', 'hc-a2': 'NY', 'x': 2, 'region': 'Northeast', 'value': 181830}, {'y': 2, 'name': 'California', 'hc-a2': 'CA', 'x': 5, 'region': 'West', 'value': 155947}, {'y': 4, 'name': 'Texas', 'hc-a2': 'TX', 'x': 7, 'region': 'South', 'value': 70821}, {'y': 6, 'name': 'Illinois', 'hc-a2': 'IL', 'x': 3, 'region': 'Midwest', 'value': 53089}, {'y': 9, 'name': 'Pennsylvania', 'hc-a2': 'PA', 'x': 3, 'region': 'Northeast', 'value': 17726}, {'y': 3, 'name': 'Arizona', 'hc-a2': 'AZ', 'x': 5, 'region': 'West', 'value': 13314}, {'y': 8, 'name': 'Florida', 'hc-a2': 'FL', 'x': 8, 'region': 'South', 'value': 6205}, {'y': 7, 'name': 'Indiana', 'hc-a2': 'IN', 'x': 3, 'region': 'Midwest', 'value': 8608}, {'y': 8, 'name': 'Ohio', 'hc-a2': 'OH', 'x': 3, 'region': 'Midwest', 'value': 6033}, {'y': 7, 'name': 'Michigan', 'hc-a2': 'MI', 'x': 2, 'region': 'Midwest', 'value': 3253}, {'y': 9, 'name': 'North Carolina', 'hc-a2': 'NC', 'x': 5, 'region': 'South', 'value': 8190}, {'y': 7, 'name': 'Tennessee', 'hc-a2': 'TN', 'x': 5, 'region': 'South', 'value': 8314}, {'y': 1, 'name': 'Washington', 'hc-a2': 'WA', 'x': 2, 'region': 'West', 'value': 29738}, {'y': 10, 'name': 'Massachusetts', 'hc-a2': 'MA', 'x': 2, 'region': 'Northeast', 'value': 33367}, {'y': 8, 'name': 'Maryland', 'hc-a2': 'MD', 'x': 4, 'region': 'South', 'value': 10429}, {'y': 3, 'name': 'Colorado', 'hc-a2': 'CO', 'x': 4, 'region': 'West', 'value': 14619}, {'y': 10, 'name': 'District of Columbia', 'hc-a2': 'DC', 'x': 4, 'region': 'South', 'value': 38243}, {'name': 'Alabama', 'hc-a2': 'AL', 'region': 'South', 'value': 0, 'y': 7, 'x': 6}, {'name': 'Alaska', 'hc-a2': 'AK', 'region': 'West', 'value': 0, 'y': 0, 'x': 0}, {'name': 'Arkansas', 'hc-a2': 'AR', 'region': 'South', 'value': 0, 'y': 6, 'x': 5}, {'name': 'Connecticut', 'hc-a2': 'CT', 'region': 'Northeast', 'value': 0, 'y': 11, 'x': 3}, {'name': 'Delaware', 'hc-a2': 'DE', 'region': 'South', 'value': 0, 'y': 9, 'x': 4}, {'name': 'Georgia', 'hc-a2': 'GA', 'region': 'South', 'value': 0, 'y': 8, 'x': 7}, {'name': 'Hawaii', 'hc-a2': 'HI', 'region': 'West', 'value': 0, 'y': 0, 'x': 8}, {'name': 'Idaho', 'hc-a2': 'ID', 'region': 'West', 'value': 0, 'y': 2, 'x': 3}, {'name': 'Iowa', 'hc-a2': 'IA', 'region': 'Midwest', 'value': 0, 'y': 5, 'x': 3}, {'name': 'Kansas', 'hc-a2': 'KS', 'region': 'Midwest', 'value': 0, 'y': 5, 'x': 5}, {'name': 'Kentucky', 'hc-a2': 'KY', 'region': 'South', 'value': 0, 'y': 6, 'x': 4}, {'name': 'Louisiana', 'hc-a2': 'LA', 'region': 'South', 'value': 0, 'y': 5, 'x': 6}, {'name': 'Maine', 'hc-a2': 'ME', 'region': 'Northeast', 'value': 0, 'y': 11, 'x': 0}, {'name': 'Minnesota', 'hc-a2': 'MN', 'region': 'Midwest', 'value': 0, 'y': 4, 'x': 2}, {'name': 'Mississippi', 'hc-a2': 'MS', 'region': 'South', 'value': 0, 'y': 6, 'x': 6}, {'name': 'Missouri', 'hc-a2': 'MO', 'region': 'Midwest', 'value': 0, 'y': 5, 'x': 4}, {'name': 'Montana', 'hc-a2': 'MT', 'region': 'West', 'value': 0, 'y': 2, 'x': 2}, {'name': 'Nebraska', 'hc-a2': 'NE', 'region': 'Midwest', 'value': 0, 'y': 4, 'x': 4}, {'name': 'Nevada', 'hc-a2': 'NV', 'region': 'West', 'value': 0, 'y': 2, 'x': 4}, {'name': 'New Hampshire', 'hc-a2': 'NH', 'region': 'Northeast', 'value': 0, 'y': 11, 'x': 1}, {'name': 'New Jersey', 'hc-a2': 'NJ', 'region': 'Northeast', 'value': 0, 'y': 10, 'x': 3}, {'name': 'New Mexico', 'hc-a2': 'NM', 'region': 'West', 'value': 0, 'y': 3, 'x': 6}, {'name': 'North Dakota', 'hc-a2': 'ND', 'region': 'Midwest', 'value': 0, 'y': 3, 'x': 2}, {'name': 'Oklahoma', 'hc-a2': 'OK', 'region': 'South', 'value': 0, 'y': 4, 'x': 6}, {'name': 'Oregon', 'hc-a2': 'OR', 'region': 'West', 'value': 0, 'y': 1, 'x': 4}, {'name': 'Rhode Island', 'hc-a2': 'RI', 'region': 'Northeast', 'value': 0, 'y': 11, 'x': 2}, {'name': 'South Carolina', 'hc-a2': 'SC', 'region': 'South', 'value': 0, 'y': 8, 'x': 6}, {'name': 'South Dakota', 'hc-a2': 'SD', 'region': 'Midwest', 'value': 0, 'y': 4, 'x': 3}, {'name': 'Utah', 'hc-a2': 'UT', 'region': 'West', 'value': 0, 'y': 4, 'x': 5}, {'name': 'Vermont', 'hc-a2': 'VT', 'region': 'Northeast', 'value': 0, 'y': 10, 'x': 1}, {'name': 'Virginia', 'hc-a2': 'VA', 'region': 'South', 'value': 0, 'y': 8, 'x': 5}, {'name': 'West Virginia', 'hc-a2': 'WV', 'region': 'South', 'value': 0, 'y': 7, 'x': 4}, {'name': 'Wisconsin', 'hc-a2': 'WI', 'region': 'Midwest', 'value': 0, 'y': 5, 'x': 2}, {'name': 'Wyoming', 'hc-a2': 'WY', 'region': 'West', 'value': 0, 'y': 3, 'x': 3}]
}];

let negativeStateData = [{
    name: '',
    data: [{'y': 9, 'name': 'New York', 'hc-a2': 'NY', 'x': 2, 'region': 'Northeast', 'value': 85227}, {'y': 2, 'name': 'California', 'hc-a2': 'CA', 'x': 5, 'region': 'West', 'value': 54317}, {'y': 4, 'name': 'Texas', 'hc-a2': 'TX', 'x': 7, 'region': 'South', 'value': 17738}, {'y': 6, 'name': 'Illinois', 'hc-a2': 'IL', 'x': 3, 'region': 'Midwest', 'value': 11442}, {'y': 9, 'name': 'Pennsylvania', 'hc-a2': 'PA', 'x': 3, 'region': 'Northeast', 'value': 5101}, {'y': 3, 'name': 'Arizona', 'hc-a2': 'AZ', 'x': 5, 'region': 'West', 'value': 2968}, {'y': 8, 'name': 'Florida', 'hc-a2': 'FL', 'x': 8, 'region': 'South', 'value': 1746}, {'y': 7, 'name': 'Indiana', 'hc-a2': 'IN', 'x': 3, 'region': 'Midwest', 'value': 2362}, {'y': 8, 'name': 'Ohio', 'hc-a2': 'OH', 'x': 3, 'region': 'Midwest', 'value': 2073}, {'y': 7, 'name': 'Michigan', 'hc-a2': 'MI', 'x': 2, 'region': 'Midwest', 'value': 747}, {'y': 9, 'name': 'North Carolina', 'hc-a2': 'NC', 'x': 5, 'region': 'South', 'value': 2376}, {'y': 7, 'name': 'Tennessee', 'hc-a2': 'TN', 'x': 5, 'region': 'South', 'value': 2553}, {'y': 1, 'name': 'Washington', 'hc-a2': 'WA', 'x': 2, 'region': 'West', 'value': 5773}, {'y': 10, 'name': 'Massachusetts', 'hc-a2': 'MA', 'x': 2, 'region': 'Northeast', 'value': 8523}, {'y': 8, 'name': 'Maryland', 'hc-a2': 'MD', 'x': 4, 'region': 'South', 'value': 2278}, {'y': 3, 'name': 'Colorado', 'hc-a2': 'CO', 'x': 4, 'region': 'West', 'value': 3517}, {'y': 10, 'name': 'District of Columbia', 'hc-a2': 'DC', 'x': 4, 'region': 'South', 'value': 10094}, {'name': 'Alabama', 'hc-a2': 'AL', 'region': 'South', 'value': 0, 'y': 7, 'x': 6}, {'name': 'Alaska', 'hc-a2': 'AK', 'region': 'West', 'value': 0, 'y': 0, 'x': 0}, {'name': 'Arkansas', 'hc-a2': 'AR', 'region': 'South', 'value': 0, 'y': 6, 'x': 5}, {'name': 'Connecticut', 'hc-a2': 'CT', 'region': 'Northeast', 'value': 0, 'y': 11, 'x': 3}, {'name': 'Delaware', 'hc-a2': 'DE', 'region': 'South', 'value': 0, 'y': 9, 'x': 4}, {'name': 'Georgia', 'hc-a2': 'GA', 'region': 'South', 'value': 0, 'y': 8, 'x': 7}, {'name': 'Hawaii', 'hc-a2': 'HI', 'region': 'West', 'value': 0, 'y': 0, 'x': 8}, {'name': 'Idaho', 'hc-a2': 'ID', 'region': 'West', 'value': 0, 'y': 2, 'x': 3}, {'name': 'Iowa', 'hc-a2': 'IA', 'region': 'Midwest', 'value': 0, 'y': 5, 'x': 3}, {'name': 'Kansas', 'hc-a2': 'KS', 'region': 'Midwest', 'value': 0, 'y': 5, 'x': 5}, {'name': 'Kentucky', 'hc-a2': 'KY', 'region': 'South', 'value': 0, 'y': 6, 'x': 4}, {'name': 'Louisiana', 'hc-a2': 'LA', 'region': 'South', 'value': 0, 'y': 5, 'x': 6}, {'name': 'Maine', 'hc-a2': 'ME', 'region': 'Northeast', 'value': 0, 'y': 11, 'x': 0}, {'name': 'Minnesota', 'hc-a2': 'MN', 'region': 'Midwest', 'value': 0, 'y': 4, 'x': 2}, {'name': 'Mississippi', 'hc-a2': 'MS', 'region': 'South', 'value': 0, 'y': 6, 'x': 6}, {'name': 'Missouri', 'hc-a2': 'MO', 'region': 'Midwest', 'value': 0, 'y': 5, 'x': 4}, {'name': 'Montana', 'hc-a2': 'MT', 'region': 'West', 'value': 0, 'y': 2, 'x': 2}, {'name': 'Nebraska', 'hc-a2': 'NE', 'region': 'Midwest', 'value': 0, 'y': 4, 'x': 4}, {'name': 'Nevada', 'hc-a2': 'NV', 'region': 'West', 'value': 0, 'y': 2, 'x': 4}, {'name': 'New Hampshire', 'hc-a2': 'NH', 'region': 'Northeast', 'value': 0, 'y': 11, 'x': 1}, {'name': 'New Jersey', 'hc-a2': 'NJ', 'region': 'Northeast', 'value': 0, 'y': 10, 'x': 3}, {'name': 'New Mexico', 'hc-a2': 'NM', 'region': 'West', 'value': 0, 'y': 3, 'x': 6}, {'name': 'North Dakota', 'hc-a2': 'ND', 'region': 'Midwest', 'value': 0, 'y': 3, 'x': 2}, {'name': 'Oklahoma', 'hc-a2': 'OK', 'region': 'South', 'value': 0, 'y': 4, 'x': 6}, {'name': 'Oregon', 'hc-a2': 'OR', 'region': 'West', 'value': 0, 'y': 1, 'x': 4}, {'name': 'Rhode Island', 'hc-a2': 'RI', 'region': 'Northeast', 'value': 0, 'y': 11, 'x': 2}, {'name': 'South Carolina', 'hc-a2': 'SC', 'region': 'South', 'value': 0, 'y': 8, 'x': 6}, {'name': 'South Dakota', 'hc-a2': 'SD', 'region': 'Midwest', 'value': 0, 'y': 4, 'x': 3}, {'name': 'Utah', 'hc-a2': 'UT', 'region': 'West', 'value': 0, 'y': 4, 'x': 5}, {'name': 'Vermont', 'hc-a2': 'VT', 'region': 'Northeast', 'value': 0, 'y': 10, 'x': 1}, {'name': 'Virginia', 'hc-a2': 'VA', 'region': 'South', 'value': 0, 'y': 8, 'x': 5}, {'name': 'West Virginia', 'hc-a2': 'WV', 'region': 'South', 'value': 0, 'y': 7, 'x': 4}, {'name': 'Wisconsin', 'hc-a2': 'WI', 'region': 'Midwest', 'value': 0, 'y': 5, 'x': 2}, {'name': 'Wyoming', 'hc-a2': 'WY', 'region': 'West', 'value': 0, 'y': 3, 'x': 3}]
}];


let stateMap = {'MD': 0.047581602429443905, 'MA': 0.15685789924997284, 'WA': 0.13297161280176142, 'TX': 0.3316108546115623, 'OH': 0.030353070692773453, 'CA': 0.7873375346836069, 'NC': 0.03956458733528797, 'MI': 0.014978075841487023, 'DC': 0.18099881298748957, 'TN': 0.04069168754235987, 'NY': 1.0, 'PA': 0.08547613430840607, 'IL': 0.24163755303174977, 'IN': 0.04107737299527816, 'CO': 0.06791059586530217, 'AZ': 0.06096825771277293, 'FL': 0.02977267025391583};


let stateRegions = [{'regions': [60763], 'state': 'NY'}, {'regions': [60713, 33020, 60750, 32655], 'state': 'CA'}, {'regions': [60768, 56003, 55857, 30196, 60956, 55711], 'state': 'TX'}, {'regions': [35805], 'state': 'IL'}, {'regions': [60795], 'state': 'PA'}, {'regions': [31310], 'state': 'AZ'}, {'regions': [60805], 'state': 'FL'}, {'regions': [37209], 'state': 'IN'}, {'regions': [50226], 'state': 'OH'}, {'regions': [42139], 'state': 'MI'}, {'regions': [49022], 'state': 'NC'}, {'regions': [55197], 'state': 'TN'}, {'regions': [60878], 'state': 'WA'}, {'regions': [60745], 'state': 'MA'}, {'regions': [60811], 'state': 'MD'}, {'regions': [33388], 'state': 'CO'}, {'regions': [28970], 'state': 'DC'}];

function getRegionsForState(state){
    let result = [];
    stateRegions.forEach(value => {
        if(value['state'] === state)
            result = value['regions'];
    });
    return result;
}
